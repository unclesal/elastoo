#!/usr/bin/env python3
from aiohttp import web

from application import worker, settings, views

if __name__ == '__main__':

    worker = worker.Worker()
    app = web.Application()

    # Маршрутизация.
    app.router.add_route('*', '/tasks', views.TaskView)
    app.router.add_route('*', '/stats', views.StatsView)

    # Запуск aiohttp сервера.
    web.run_app(
        app=app,
        host=settings.HOST,
        port=settings.PORT
    )
