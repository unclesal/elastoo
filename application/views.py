import json
from aiohttp import web

from application import utils


class TaskView(web.View):
    """ API задач.
    GET - получение очереди задач
    PUT - добавление задачи.
    """

    def __init__(self, request: web.Request):
        super().__init__(request)
        self._queue = utils.QueueList()

    async def get(self):
        """ GET - получение очереди задач.
        """
        queue = await self._queue.get_all_queue()
        return web.Response(
            body=json.dumps(queue),
            content_type='application/json'
        )

    async def put(self):
        """ PUT - добавить новую задачу в очередь.
        """
        the_task = await self.request.json()

        # Вот оно не то чтобы "валидация", но worker будет спать сколько-то.
        # И не хотелось бы, чтобы это было "неведомо сколько". При желании
        # валидацию можно сделать на чем-нибудь, вместе с преобразованием
        # из JSON'а в объекты и наоборот. Например, на ValidX.

        await self._queue.append({
            'num': the_task.get('num', 0),
            'timeout': the_task.get('timeout', 1)
        })

        return web.Response(
            body=json.dumps({
                'message': 'Ok'
            }),
            content_type='application/json'
        )


class StatsView(web.View):
    """ Статистика (список выполненных задач)

    """
    def __init__(self, request: web.Request):
        super().__init__(request)
        self._stats = utils.StatStoreList()

    async def get(self):
        stats = await self._stats.get()

        return web.Response(
            # Согласно постановке, в статистике должны
            # быть - только номера выполненных задач.
            # (п. 2 описания API в задании)
            body=json.dumps(
                [s['index'] for s in stats]
            ),
            content_type='application/json'
        )
