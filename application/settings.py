import os
import dotenv

dotenv.load_dotenv(verbose=True)

# Хост и порт, куда садится сервер.
HOST = os.getenv('HOST', '127.0.0.1')
PORT = os.getenv('PORT', '8080')
