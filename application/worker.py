""" Worker, выполняющий задачи.

"""
import asyncio

from . import utils


class Worker:

    def __init__(self):
        asyncio.ensure_future(self.run())
        self._queue = utils.QueueList()
        self._stats = utils.StatStoreList()

    async def run(self):
        """ Метод выполнения worker'а.
        """
        while True:

            t: dict = await self._queue.get_first()
            print(f'Worker: got task: {t}')
            # Спим, сколько предусмотрено задачей.
            await asyncio.sleep(t['timeout'])
            # Задача выполнена. Удаляем ее из очереди, одновременно
            # добавляя ее в статистику выполненных задач.
            await self._queue.remove_first()
            await self._stats.append(t)
