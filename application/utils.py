""" Общие утилиты.

"""
import asyncio
import copy

from typing import List, Dict
from datetime import datetime

# Singleton очереди задач.

# Здесь не устроит стандатная asyncio.Queue, т.к. одна из endpoint api - это
# вся очередь сообщений. Плюс, по условию задания, из нее надо удалять только
# после выполнения задачи. Данная реализация имеет существенный минус, к
# singleton'ам все-таки можно обратиться извне и непосредственно, минуя
# их "обертки". Если это критично, то можно сами объекты "закопать" поглубже
# через замыкание или дескрипторы.

_QUEUE: List[Dict] = list()

# Singleton результатов выполнения задач.
_STAT_STORE: List[Dict] = list()


class QueueList:
    """ Простейший "заменитель" очереди.

    Использовать стандартную asyncio.Queue - нельзя, т.к. она не дает возможности
    свободного управления елементами.

    """
    def __init__(self):
        # Залочка. По-идее - излишняя в данном случае, т.к. мы говорим об одном
        # воркере и одном процессе. Посажена - ну, в нагруженных системах лучше
        # бы напрямую к одному экземпляру переменной не обращаться, все-таки
        # подстраховаться как-то.
        self._lock = asyncio.Lock()

    async def get_first(self) -> dict:
        """ Получить первый элемент из очереди задач.

        Не сильно хорошее решение, т.к. использует sleep. Для "боевого" варианта нужно
        что-то другое, посерьезнее. Например, RabbitMQ. Это - просто как демка.

        Returns: Первую в очереди задачу на выполнение.

        """
        global _QUEUE
        while True:

            await self._lock.acquire()

            if len(_QUEUE) > 0:
                elem = _QUEUE[0]
                self._lock.release()
                return elem

            self._lock.release()

            # Вот это вот - корявость. Но для демки, я думаю, сойдет.
            # Все равно в "настоящем" варианте не будет таких вот
            # примитивных очередей на worker'ы. 0 здесь ставить
            # нельзя, иначе займем весь процессор полностью -
            # только переключением между задачами.

            await asyncio.sleep(0.001)

    async def remove_first(self):
        """ Удалить в очереди задач первый элемент, т.е. элемент с индексом 0
        """
        global _QUEUE
        await self._lock.acquire()
        if not _QUEUE:
            self._lock.release()
            raise Exception('QueueList:remove_first(), queue is empty.')

        _QUEUE = _QUEUE[1:]
        self._lock.release()

    async def get_all_queue(self) -> list:
        """ Вернуть полную копию очереди.

        Returns: Копия очереди задач, ожидающих выполнения.

        """
        global _QUEUE
        await self._lock.acquire()
        result = copy.copy(_QUEUE)
        self._lock.release()
        return result

    async def append(self, t: dict):
        """ Добавить задачу в очередь задач на выполнение.

        Args:
            t: задача, которую добавляем в очередь.
        """
        global _QUEUE
        await self._lock.acquire()

        # "Номер в истории". Предположим, что номера в истории у нас будут с 1.
        index = len(_QUEUE) + 1
        t['index'] = index

        # Времена удобно хранить в UTC timestamp, в этом случае они
        # на клиенте в JavaScript в одно движение преобразуются в
        # локальный формат даты с учетом временнОй зоны данного клиента.
        t['created'] = datetime.utcnow().timestamp()

        _QUEUE.append(t)
        self._lock.release()


class StatStoreList:
    """ "Статистика" - список выполненых задач.
    """

    def __init__(self):
        self._lock = asyncio.Lock()

    async def append(self, t: dict):
        """ Добавить в "журнал выполненных задач" следующую выполненную задачу.

        Args:
            t: Задача, которая только что была выполнена.

        Returns:

        """
        global _STAT_STORE
        await self._lock.acquire()
        _STAT_STORE.append(t)
        self._lock.release()

    async def get(self) -> list:
        """ Получить копию списка выполненных задач.

        Returns: Список выполненных задач с их порядковым номером.

        """
        await self._lock.acquire()
        result = copy.copy(_STAT_STORE)
        self._lock.release()
        return result
