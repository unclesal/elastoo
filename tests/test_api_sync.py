""" Синхронное тестирование API сервера.

"""
import pytest
import json
import time
import requests

from application import settings


class TestAPISync:

    url = f'http://{settings.HOST}:{settings.PORT}'

    def check_item(self, item: dict):
        """ Проверка задачи, которую мы давали.
        Вынесена в отдельную процедуру, т.к. выполняется дважды - для очереди и для истории.

        Args:
            item: dict-представление задачи.
        """

    @pytest.mark.sync
    def test_api(self):
        # Создаем задачу.
        response = requests.put(
            f'{self.url}/tasks', json.dumps({
                'num': 1,
                'timeout': 5
            }),
            headers={
                'Content-Type': 'application/json'
            }
        )
        assert response.status_code == 200

        # В задаче мы будем ждать целых 5 секунд. Этого достаточно,
        # чтобы успеть выгрести очередь, в которой еще что-то есть.

        response = requests.get(f'{self.url}/tasks')
        assert response.status_code == 200
        queue = response.json()
        assert queue
        assert isinstance(queue, list)
        item = queue[0]
        num = item.get('num', -1)
        timeout = item.get('timeout', -1)
        # Добавленный при складывании в очередь "номер в истории"
        index = item.get('index', -1)
        assert index == 1
        assert num == 1
        assert timeout == 5

        # Теперь ждем эти самые 5 секунд. Лучше с запасом.
        # За это время worker должен "отработать" данную задачу.
        time.sleep(6)

        # Очередь задач на выполнение должна быть пустой.
        response = requests.get(f'{self.url}/tasks')
        assert response.status_code == 200
        queue = response.json()
        assert len(queue) == 0

        # А вот в статистике должна появиться данная
        # задача в качестве "отработанных".

        response = requests.get(f'{self.url}/stats')
        assert response.status_code == 200
        stats = response.json()
        assert stats
        assert isinstance(stats, list)
        assert 1 in stats
